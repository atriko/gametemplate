using System;
using System.Collections;
using System.IO;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

[Serializable]
public class ScreenshotsEditor
{
#if UNITY_EDITOR
    [Title("General")] public bool ShowLogOnCreation = true;
    public bool OpenOnFinish = true;
    public bool IsScreenShotMode = false;

    [Title("Camera")] public Camera Camera;
    //public bool IsTransparent = false;

    [Title("Screen Size")] public int ScreenWidth;
    public int ScreenHeight;

    [Button(ButtonSizes.Medium)]
    public void SetAsGameScreenSize()
    {
        ScreenWidth = (int) Handles.GetMainGameViewSize().x;
        ScreenHeight = (int) Handles.GetMainGameViewSize().y;
    }

    [Title("Save Path")] [InlineButton(nameof(BrowseSavePath), "Browse")]
    public string SavePath;

    public bool SeperateToResolutionFolders = false;

    public void BrowseSavePath()
    {
        SavePath = EditorUtility.SaveFolderPanel("Path to Save Images", SavePath, Application.dataPath);
    }

    private IEnumerator m_ScreenshotCoroutine;

    public Tuple<int, int> GetCurrentScreenSize()
    {
        return new Tuple<int, int>((int) Handles.GetMainGameViewSize().x, (int) Handles.GetMainGameViewSize().y);
    }

    // iPhone
    private readonly Tuple<int, int, string> r_Iphone8 =
        new Tuple<int, int, string>(1284, 2778, "AppStore/1284x2778 - Iphone 6.5");

    private readonly Tuple<int, int, string> r_Iphone5 =
        new Tuple<int, int, string>(1242, 2208, "AppStore/1242x2208 - Iphone 5.5");

    private readonly Tuple<int, int, string> r_IphoneIpad =
        new Tuple<int, int, string>(2048, 2732, "AppStore/2048x2732 - iPAD");

    [Button(ButtonSizes.Medium)]
    public void TakeScreenshot()
    {
        TakeScreenShot(new[]
            {new Tuple<int, int, string>(ScreenWidth, ScreenHeight, $"{ScreenWidth}x{ScreenHeight}")});
    }
    [Button("Take All Screenshots At Once (iPhone)", ButtonSizes.Large)]
    public void TakeAllScreenshotsAtOnceIphone()
    {
        TakeScreenShot(new[] {r_Iphone8, r_Iphone5, r_IphoneIpad});
    }
    private void TakeScreenShot(Tuple<int, int, string>[] i_Resolutions)
    {
        if (SavePath == string.Empty)
            BrowseSavePath();

        if (SavePath != string.Empty)
        {
            if (EditorApplication.isPaused || !EditorApplication.isPlaying)
            {
                EditorApplication.update += coroutineHandler;
                m_ScreenshotCoroutine = TakeScreenShotCo(i_Resolutions);
            }
            else
            {
                ScreenshotsManager.Instance.StartCoroutine(TakeScreenShotCo(i_Resolutions));
            }
        }
    }

    void coroutineHandler()
    {
        m_ScreenshotCoroutine.MoveNext();
    }

    private IEnumerator TakeScreenShotCo(Tuple<int, int, string>[] i_Resolutions)
    {
        string path = string.Empty;

        Time.timeScale = 0;

        QualitySettings.antiAliasing = 8;

        foreach (Tuple<int, int, string> resolution in i_Resolutions)
        {
            path = ScreenShotName(resolution, SavePath, SeperateToResolutionFolders);
            GameViewSize.BackupCurrentSize();
            GameViewSize.SelectSize(GameViewSize.SetCustomSize(resolution.Item1, resolution.Item2));

            yield return new WaitForSecondsRealtime(.03f);

            if (Application.isPlaying && EditorApplication.isPaused)
                EditorApplication.Step();

            ScreenCapture.CaptureScreenshot(path);

            yield return new WaitForSecondsRealtime(.01f);

            if (Application.isPlaying && EditorApplication.isPaused)
                EditorApplication.Step();

            yield return new WaitForSecondsRealtime(.01f);

            GameViewSize.RestoreSize();
        }

        if (ShowLogOnCreation)
            Debug.LogErrorFormat(string.Format("Finished taking {0} screenshots", i_Resolutions.Length));

        if (OpenOnFinish && i_Resolutions.Length == 1)
            Application.OpenURL(path);

        EditorApplication.update -= coroutineHandler;

        QualitySettings.antiAliasing = 0;

        Time.timeScale = 1;
    }

    public string ScreenShotName(Tuple<int, int, string> i_Resolution, string i_Path,
        bool i_SeperateToResolutionFolders)
    {
        string strPath = string.Empty;
        string filename = string.Format("{0}x{1}_{2}.png", i_Resolution.Item1, i_Resolution.Item2,
            DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));

        if (i_SeperateToResolutionFolders)
        {
            strPath = string.Format("{0}/{1}", i_Path, i_Resolution.Item3);
        }
        else
        {
            strPath = i_Path;
        }

        if (!Directory.Exists(strPath)) Directory.CreateDirectory(strPath);

        strPath = $"{strPath}/{filename}";

        return strPath;
    }

    [OnInspectorGUI]
    void OnInspector()
    {
        if (Camera == null) Camera = GameObject.FindObjectOfType<Camera>();

        if (ScreenWidth == 0 || ScreenHeight == 0)
            SetAsGameScreenSize();
    }

#endif
}