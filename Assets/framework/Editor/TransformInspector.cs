﻿using System;
using UnityEditor;
using UnityEngine;

namespace Framework.Editor
{
    [CustomEditor(typeof(Transform))]
    public class TransformInspector : UnityEditor.Editor
    {
        public Texture lockScaleTexture;
        public Texture unlockScaleTexture;
        private static bool lockScale;

        public override void OnInspectorGUI()
        {
            Transform t = (Transform)target;
            GUI.color = Color.white;

            EditorGUI.indentLevel = 0;
            Vector3 position = EditorGUILayout.Vector3Field("Position", t.localPosition);
            Vector3 eulerAngles = EditorGUILayout.Vector3Field("Rotation", t.localEulerAngles);

            EditorGUI.BeginChangeCheck();
            Vector3 scale = EditorGUILayout.Vector3Field("Scale", t.localScale);

            Rect scaleFieldRect = GUILayoutUtility.GetLastRect();
            Rect toggleRect = new Rect(scaleFieldRect.x + EditorGUIUtility.labelWidth - 20f, scaleFieldRect.y, 20f, EditorGUIUtility.singleLineHeight);

            lockScale = GUI.Toggle(toggleRect, lockScale, lockScale ? lockScaleTexture : unlockScaleTexture, GUIStyle.none);

            if (EditorGUI.EndChangeCheck())
            {
                if (lockScale)
                {
                    if (t.localScale.x != scale.x)
                    {
                        scale.y = scale.z = scale.x;
                    }
                    else if (t.localScale.y != scale.y)
                    {
                        scale.x = scale.z = scale.y;
                    }
                    else if (t.localScale.z != scale.z)
                    {
                        scale.x = scale.y = scale.z;
                    }
                }
            }

            if (GUI.changed)
            {
                Undo.RecordObject(t, "Transform Change");
                t.localPosition = FixIfNaN(position);
                t.localEulerAngles = FixIfNaN(eulerAngles);
                t.localScale = FixIfNaN(scale);
            }

            GUILayout.BeginHorizontal();

            GUI.color = new Color(0.5f, 0.8f, 1f);
            if (GUILayout.Button("Reset Position"))
            {
                
                Undo.RecordObject(t, "Reset Position " + t.name);
                t.transform.position = Vector3.zero;
            }

            if (GUILayout.Button("Reset Rotation"))
            {
                Undo.RecordObject(t, "Reset Rotation " + t.name);
                t.transform.rotation = Quaternion.identity;
            }

            if (GUILayout.Button("Reset Scale"))
            {
                Undo.RecordObject(t, "Reset Scale " + t.name);
                t.transform.localScale = Vector3.one;
            }
            GUILayout.EndHorizontal();

            GUI.color = new Color(0.74f, 1f, 0.4f);
            if (GUILayout.Button("Copy Component"))
            {
                UnityEditorInternal.ComponentUtility.CopyComponent(t.transform);
            }

            if (GUILayout.Button("Paste Component"))
            {
                UnityEditorInternal.ComponentUtility.PasteComponentValues(t.transform);
            }
        }

        private Vector3 FixIfNaN(Vector3 v)
        {
            if (float.IsNaN(v.x)) v.x = 0;
            if (float.IsNaN(v.y)) v.y = 0;
            if (float.IsNaN(v.z)) v.z = 0;
            return v;
        }
    }
}