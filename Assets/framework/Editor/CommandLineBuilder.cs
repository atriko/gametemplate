﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CommandLineBuilder 
{
    static void Build()
    {
        Debug.Log("Starting build");
        
        BuildPipeline.BuildPlayer(EditorBuildSettings.scenes,
            $"{Application.dataPath}/../build/{EditorUserBuildSettings.activeBuildTarget.ToString()}",
            EditorUserBuildSettings.activeBuildTarget, BuildOptions.None);
    }
}
