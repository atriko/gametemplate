﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace DHFramework.Editor
{
    public class ScriptableObjectGeneration : EditorWindow
    {
        // [MenuItem("DHFramework/Scriptable Object Generation")]
        // static void Init()
        // {
        //     var panel = (ScriptableObjectGeneration) GetWindow(typeof(ScriptableObjectGeneration));
        //     panel.Show();
        //
        //     className = GetCurrentClassName();
        // }

        private string templateFileName = "ScriptableObjectTemplate";
        private string templatePath = String.Empty;
        private static string className = String.Empty;
        private static string classNameKey = "class_name";

        private void OnGUI()
        {
            EditorGUILayout.LabelField("Class Name");
            className = EditorGUILayout.TextField("", className);
            GUI.color = Color.green;
            if (GUILayout.Button("Generate", GUILayout.Height(25))) Generate();
        }

        private void Generate()
        {
            if (!string.IsNullOrEmpty(className.Trim()))
            {
                SaveCurrentClassName();
                GatherFilePath();
                
                string targetFilePath = $"{Application.dataPath}/{className}.cs";
                Directory.CreateDirectory(Path.GetDirectoryName(targetFilePath));
                File.WriteAllText(targetFilePath, GetScriptContents());
                AssetDatabase.Refresh();
            }
        }

        private string GetScriptContents()
        {
            string templateContent = File.ReadAllText(templatePath);
            return templateContent.Replace("$CLASS_NAME$", className);
        }

        private void GatherFilePath()
        {
            string assetPath = $"{Application.dataPath}";
            string folderToStartSearch = Directory.GetParent(assetPath).FullName;

            Queue<string> foldersToCheck = new Queue<string>();
            foldersToCheck.Enqueue(folderToStartSearch);

            while (foldersToCheck.Count > 0)
            {
                string currentDirectory = foldersToCheck.Dequeue();

                foreach (string filePath in Directory.GetFiles(currentDirectory))
                {
                    string fileName = Path.GetFileName(filePath);
                    if (string.Equals(fileName, templateFileName)) templatePath = filePath;
                }

                foreach (string subDirectory in Directory.GetDirectories(currentDirectory))
                {
                    foldersToCheck.Enqueue(subDirectory);
                }
            }
        }

        private void SaveCurrentClassName() => PlayerPrefs.SetString(classNameKey, className);
        private static string GetCurrentClassName() => PlayerPrefs.GetString(classNameKey, String.Empty);
    }
}