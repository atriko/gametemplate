﻿using GameAssets.Scripts.Level;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace DHFramework.Editor
{
    public class LevelLoaderEditor
    {
        [MenuItem("Redelic/Open First Scene %&g")]
        public static void ShowGameScene()
        {
            if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
            {
                EditorSceneManager.OpenScene(EditorBuildSettings.scenes[0].path);
            }
        }
        [MenuItem("Redelic/Fast Play/Enable")]
        public static void EnableFastPlay()
        {
            EditorSettings.enterPlayModeOptionsEnabled = true;
        }
        [MenuItem("Redelic/Fast Play/Disable")]
        public static void DisableFastPlay()
        {
            EditorSettings.enterPlayModeOptionsEnabled = false;
        }
        [MenuItem("Redelic/Levels/Reset Levels %&r")]
        public static void ResetLevels()
        {
            BoardController.CurrentSubLevelIndex = 0;
            GetLevelSystem().ResetLevels();
            GetLevelLoader().RestartLevel();

            Debug.Log("<color=green>Successfully Reset Levels</color>");
        }

        [MenuItem("Redelic/Levels/Load Previous Level #q")]
        public static void LoadPreviousLevel()
        {
            GetLevelLoader().LoadPreviousLevel();

            Debug.Log("<color=green>Successfully Loaded Previous Level</color>");
        }

        [MenuItem("Redelic/Levels/Restart Current Level #w")]
        public static void RestartCurrentLevel()
        {
            GetLevelLoader().RestartLevel();
            Debug.Log("<color=green>Successfully Restarted Level</color>");
        }

        [MenuItem("Redelic/Levels/Load Next Level #e")]
        public static void LoadNextLevel()
        {
            GetLevelLoader().LoadNextLevel();
            Debug.Log("<color=green>Successfully Loaded Next Level</color>");
        }

        private static LevelLoader levelLoader;
        private static LevelSystem levelSystem;
        private static TutorialManager tutorialManager;

        private static LevelSystem GetLevelSystem()
        {
            if (levelSystem == null) levelSystem = Resources.Load("GameLevelSystem") as LevelSystem;
            return levelSystem;
        }

        private static LevelLoader GetLevelLoader()
        {
            if (levelLoader == null) levelLoader = Resources.Load("LevelLoader") as LevelLoader;
            return levelLoader;
        }

        private static TutorialManager GetTutorialManager()
        {
            if (tutorialManager == null) tutorialManager = Resources.Load("TutorialManager") as TutorialManager;
            return tutorialManager;
        }
    }
}