using System;
using DHFramework.Game.LevelSystem;
using ScriptableObjectArchitecture;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(menuName = "Framework/Game/" + nameof(LevelSystem))]
public class LevelSystem : SerializedScriptableObject
{
    private const string levelKey = "level";
    private const string virtualLevel = "virtualLevel";

    [SerializeField] protected Level[] levels;
    public Level Current => levels[currentLevelIndex];

    [SerializeField, PropertyRange(0, "MaxLevelIndex")]
    protected int restartLevelIndex;

    private int MaxLevelIndex()
    {
        return levels.Length - 1;
    }

    private int currentLevelIndex;

    public int CurrentLevelIndex
    {
        get => currentLevelIndex;
        private set
        {
            currentLevelIndex = value;
            PlayerPrefs.SetInt(levelKey, value);
        }
    }

    private int virtualLevelIndexForUI;

    public int VirtualLevelIndexForUI
    {
        get => virtualLevelIndexForUI;
        private set
        {
            virtualLevelIndexForUI = value;
            PlayerPrefs.SetInt(virtualLevel, value);
        }
    }

    private void OnEnable()
    {
        currentLevelIndex = PlayerPrefs.GetInt(levelKey, 0);
        VirtualLevelIndexForUI = PlayerPrefs.GetInt(virtualLevel, 0);
    }

    public int TotalLevelCount => levels.Length;

    public void ResetLevels()
    {
        CurrentLevelIndex = 0;
        VirtualLevelIndexForUI = 0;
    }
    public void SetLevel(int level)
    {
        CurrentLevelIndex = level;
        VirtualLevelIndexForUI = level;
    }
    public void ProceedToNextLevel()
    {
        if (CurrentLevelIndex == levels.Length - 1)
        {
            CurrentLevelIndex = restartLevelIndex;
        }
        else
        {
            CurrentLevelIndex++;
        }

        VirtualLevelIndexForUI++;
    }

    public void ProceedToPreviousLevel()
    {
        VirtualLevelIndexForUI = Math.Max(0, VirtualLevelIndexForUI - 1);

        if (CurrentLevelIndex == restartLevelIndex && VirtualLevelIndexForUI != 0 && VirtualLevelIndexForUI % (levels.Length - 1) == 0)
        {
            CurrentLevelIndex = levels.Length - 1;
        }
        else if (CurrentLevelIndex == 0)
        {
            CurrentLevelIndex = 0;
        }
        else
        {
            CurrentLevelIndex--;
        }
    }
}