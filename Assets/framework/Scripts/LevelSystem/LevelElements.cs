﻿using UnityEngine.SceneManagement;

namespace GameAssets.Scripts.Level
{
    public class LevelElements
    {
        public Scene Scene { get; }
        public EnvironmentRoot EnvironmentRoot { get; }
        public int LevelIndex { get; }

        public LevelElements(int index, Scene scene, EnvironmentRoot environmentRoot)
        {
            LevelIndex = index;
            Scene = scene;
            EnvironmentRoot = environmentRoot;
        }
    }
}