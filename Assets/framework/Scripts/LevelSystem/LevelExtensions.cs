﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameAssets.Scripts.Level
{
    public static class LevelExtensions
    {
        public static LevelElements GetNextLevelElements(this Scene scene, int index)
        {
            GameObject[] roots = scene.GetRootGameObjects();

            EnvironmentRoot environmentRoot = null;

            GameObject baseObj = Array.Find(roots, o =>
            {
                EnvironmentRoot root = o.GetComponent<EnvironmentRoot>();
                if (root == null) return false;
                environmentRoot = root;
                return true;
            });

            return new LevelElements(index, scene, environmentRoot);
        }

        public static void LoadLevelAsync(int index, Action<Scene> onCompleted)
        {
            AsyncOperation operation = SceneManager.LoadSceneAsync($"Level{index}", LoadSceneMode.Additive);
            operation.completed += async => onCompleted.Invoke(SceneManager.GetSceneByName($"Level{index}"));
        }
    }
}