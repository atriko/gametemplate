using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using GameAssets.Scripts;
public class PooledObject : MonoBehaviour
{
    [SerializeField] private float lifeTime;
    private string poolName;

    public void Spawn(string pool, Vector3 position, Quaternion rotation)
    {
        poolName = pool;
        transform.position = position;
        transform.rotation = rotation;
        gameObject.SetActive(true);

        if (lifeTime > 0)
        {
            Observable.Timer(System.TimeSpan.FromSeconds(lifeTime)).TakeUntilDisable(this).Subscribe(l => Kill());
        }
    }

    public void Kill()
    {
        PoolManager.Instance.ReturnToPool(gameObject);
    }

}
