using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameAssets.Scripts
{
    [CreateAssetMenu(fileName = "MusicPlayer", menuName = "Game/MusicPlayer")]
    public class MusicPlayer : SingletonScriptableObject<MusicPlayer>
    {
        public float basePitch;
        public float maxPitchStreak;
        public float pitchPerLevel;
        public GameAudio[] soundList;
    }
}