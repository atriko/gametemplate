using System;
using System.Collections.Generic;
using GameAssets.Scripts;
using Sirenix.Utilities;
using UniRx;
using UnityEngine;

[Serializable]
public struct GameAudio
{
    public string name;
    public AudioClip clip;
    public bool loop;
    public float volume;
}

public class SoundManager : MonoSingleton<SoundManager>
{
    private Dictionary<string, AudioSource> audios = new Dictionary<string, AudioSource>();

    public bool SoundEnabled
    {
        get => PlayerPrefs.GetInt("SoundStatus", 1) == 1;
        set => PlayerPrefs.SetInt("SoundStatus", value ? 1 : 0);
    }

    private void Start()
    {
        if (Instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

        foreach (var sound in MusicPlayer.Instance.soundList)
        {
            var source = gameObject.AddComponent<AudioSource>();
            source.clip = sound.clip;
            source.loop = sound.loop;
            source.volume = sound.volume;
            audios.Add(sound.name, source);
        }
        PlayMusic("BackgroundMusic");
    }

    public void PlaySound(string soundName, float pitch = 1f)
    {
        if (!SoundEnabled)
            return;
        audios.TryGetValue(soundName, out var audioSource);
        if (audioSource != null)
        {
            audioSource.pitch = pitch;
            audioSource.Play();
        }
    }

    private void PlayMusic(string soundName)
    {
        if (!SoundEnabled)
            return;
        audios.TryGetValue(soundName, out var audioSource);
        if (audioSource != null)
        {
            if (audioSource.isPlaying)
            {
                return;
            }
            audioSource.Play();
        }
    }
    public void UpdateSound(bool status)
    {
        SoundEnabled = status;
        if (!SoundEnabled)
        {
            audios.ForEach(pair => pair.Value.Stop());
        }
        else
        {
            PlayMusic("BackgroundMusic");
        }
    }
    private IDisposable disposable;
    public void PlayWinSound()
    {
        audios.ForEach(pair => pair.Value.Stop());
        PlaySound("LevelWin");
        audios.TryGetValue("LevelWin", out var winAudio);
        disposable = Observable.EveryUpdate().TakeUntilDisable(this).Subscribe(delegate(long l)
        {
            if (winAudio != null)
            {
                if (!winAudio.isPlaying)
                {
                    disposable?.Dispose();
                    PlayMusic("BackgroundMusic");
                }
            }
        });
    }
}