using System;
using GameAssets.Scripts;
using Sirenix.Utilities;
using UnityEngine;
// using VoxelBusters.EssentialKit;
// using NotificationServices = VoxelBusters.EssentialKit.NotificationServices;

public class NotificationManager : MonoSingleton<NotificationManager>
{
    // private static bool notificationsReady;
    //
    // private void Start()
    // {
    //     if (notificationsReady)
    //         return;
    //     NotificationServices.GetSettings(result =>
    //     {
    //         if (result.Settings.PermissionStatus == NotificationPermissionStatus.Authorized)
    //         {
    //             RescheduleAll();
    //         }
    //         else
    //         {
    //             var currentLevelNumber = LevelManager.Instance.GetCurrentLevelNumber();
    //             if (currentLevelNumber % GameNotificationSettings.Instance.notificationPopupLevelInterval == 0 && PlayerPrefs.GetInt($"NotificationAsked{currentLevelNumber}", 0) == 0)
    //             {
    //                 UIManager.Instance.ShowPanelAdditive(PanelType.Notification);
    //             }
    //         }
    //     });
    // }
    //
    // private void RequestPermission()
    // {
    //     NotificationServices.RequestPermission(
    //         NotificationPermissionOptions.Alert | NotificationPermissionOptions.Sound |
    //         NotificationPermissionOptions.Badge | NotificationPermissionOptions.ProvidesAppNotificationSettings,
    //         callback: (result, error) => { NotificationPermissionResult(); });
    // }
    //
    // private void RescheduleAll()
    // {
    //     NotificationServices.RemoveAllDeliveredNotifications();
    //     NotificationServices.CancelAllScheduledNotifications();
    //
    //     GameNotificationSettings.Instance.notifications.ForEach(delegate(Notification notification)
    //     {
    //         for (int i = 0; i < 7; i++)
    //         {
    //             ScheduleNotification(notification.title,notification.message,DateTime.Today.AddHours(notification.hour).AddMinutes(notification.minute).AddDays(i));
    //         }
    //     });
    //     notificationsReady = true;
    // }
    //
    // private void NotificationPermissionResult()
    // {
    //     NotificationServices.GetSettings(result =>
    //     {
    //         if (result.Settings.PermissionStatus == NotificationPermissionStatus.Authorized)
    //         {
    //             var currentLevelNumber = LevelManager.Instance.GetCurrentLevelNumber();
    //             PlayerPrefs.SetInt($"NotificationAsked{currentLevelNumber}", 1);
    //             RescheduleAll();
    //             UIManager.Instance.HidePanel(PanelType.Notification);
    //         }
    //         else if (result.Settings.PermissionStatus == NotificationPermissionStatus.Denied)
    //         {
    //             var currentLevelNumber = LevelManager.Instance.GetCurrentLevelNumber();
    //             PlayerPrefs.SetInt($"NotificationAsked{currentLevelNumber}", 1);
    //             UIManager.Instance.HidePanel(PanelType.Notification);
    //         }
    //     });
    // }
    //
    // private void ScheduleNotification(string notificationTitle, string notificationMessage,DateTime targetTime)
    // {
    //     var now = DateTime.Now;
    //     var difference = targetTime - now;
    //     if (difference.TotalSeconds <= 0)
    //     {
    //         return;
    //     }
    //     string id = $"Timed Notification For {targetTime}";
    //     string title = $"{notificationTitle}";
    //     string body = $"{notificationMessage}";
    //
    //     var notification = NotificationBuilder.CreateNotification(id).SetTitle(title).SetBody(body).SetTimeIntervalNotificationTrigger(difference.TotalSeconds).Create();
    //
    //     NotificationServices.ScheduleNotification(notification, (success, error) =>
    //     {
    //         if (success)
    //         {
    //         }
    //     });
    // }
    //
    // public void AcceptClicked()
    // {
    //     RequestPermission();
    // }
}