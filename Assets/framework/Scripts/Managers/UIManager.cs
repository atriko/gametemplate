using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts;
using Sirenix.Utilities;
using UnityEngine;

public enum PanelType
{
    Main,
    InGame,
    Win,
    Lose,
    Notification,
    Settings,
    Daily,
    Collection
}

public class UIManager : MonoSingleton<UIManager>
{
    private List<PanelBase> allPanels;
    private Canvas canvas;

    public bool StartFromMainMenu
    {
        get => PlayerPrefs.GetInt("StartFromMainMenu", 0) == 1;
        set => PlayerPrefs.SetInt("StartFromMainMenu", value ? 1 : 0);
    }
    private void Awake()
    {
        canvas = GetComponent<Canvas>();
        allPanels = GetComponentsInChildren<PanelBase>(true).ToList();
    }
    private void Start()
    {
        if (StartFromMainMenu)
        {
            ShowPanel(PanelType.Main);
        }
        else
        {
            ShowPanel(PanelType.InGame);
            GameManager.Instance.StartGame();
        }
    }
    public void ShowPanel(PanelType type)
    {
        HideAllPanels();
        allPanels.First(x => x.Type == type).gameObject.SetActive(true);
    }
    public void ShowPanelAdditive(PanelType type)
    {
        var panel = allPanels.FirstOrDefault(x => x.Type == type);
        if (panel != null)
        {
            panel.gameObject.SetActive(true);
        }
        else
        {
            Debug.LogError("Panel not found");
        }
    }

    public PanelBase GetPanel(PanelType type)
    {
        return allPanels.Find(panel => panel.Type.Equals(type));
    }

    public void HidePanel(PanelType type)
    {
        allPanels.First(x => x.Type == type).gameObject.SetActive(false);
    }

    public void HideAllPanels()
    {
        foreach (var panel in allPanels)
        {
            panel.gameObject.SetActive(false);
        }
    }
    
    public string ToOrdinal(int rank)
    {
        switch (rank % 100)
        {
            case 11:
            case 12:
            case 13:
                return  rank + "th";
        }
        switch (rank % 10)
        {
            case 1:
                return  rank + "st";
            case 2:
                return  rank + "nd";
            case 3:
                return  rank + "rd";
            default:
                return  rank + "th";
        }
    }

    public void UpdateMoveCount()
    {
        GetPanel(PanelType.InGame).UpdatePanel();
    }
    
    public Vector3 WorldToUISpace(Vector3 worldPos)
    {
        Vector3 screenPos = Camera.main.WorldToScreenPoint(worldPos);
        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform, screenPos, canvas.worldCamera, out Vector2 movePos);
        return canvas.transform.TransformPoint(movePos);
    }

    public bool IsAnyPopupOpen()
    {
        var status = allPanels.Count(panel => panel.Type != PanelType.InGame && panel.gameObject.activeInHierarchy) > 0;
        return status;
    }
}