﻿using Lofelt.NiceVibrations;
using UnityEngine;

namespace GameAssets.Scripts
{
    public enum HapticType { Light, Medium, Heavy, Success, Failure }
    public class HapticManager : MonoSingleton<HapticManager>
    {
        public bool HapticEnabled
        {
            get => PlayerPrefs.GetInt("HapticStatus", 1) == 1;
            set => PlayerPrefs.SetInt("HapticStatus", value ? 1 : 0);
        }
        public void HapticFeedback(HapticType type)
        {
            if (!HapticEnabled)
                return;
            HapticPatterns.PresetType hapticFeedback = HapticPatterns.PresetType.LightImpact;
            switch (type)
            {
                case HapticType.Light:
                    hapticFeedback = HapticPatterns.PresetType.LightImpact;
                    break;
                case HapticType.Medium:
                    hapticFeedback = HapticPatterns.PresetType.MediumImpact;
                    break;
                case HapticType.Heavy:
                    hapticFeedback = HapticPatterns.PresetType.HeavyImpact;
                    break;
                case HapticType.Success:
                    hapticFeedback = HapticPatterns.PresetType.Success;
                    break;
                case HapticType.Failure:
                    hapticFeedback = HapticPatterns.PresetType.Failure;
                    break;
            }
            HapticPatterns.PlayPreset(hapticFeedback);
        }

        public void UpdateHaptic(bool b)
        {
            HapticEnabled = b;
        }
    }
}