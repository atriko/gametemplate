using GameAssets.Scripts.Level;
using UnityEngine;

public class LevelManager : MonoSingleton<LevelManager>
{
   [SerializeField] private LevelLoader levelLoader;
   [SerializeField] private GameLevelSystem levelSystem;
   
   public void LoadNextLevel()
   {
      levelLoader.LoadNextLevel();
   }
   public void RestartLevel()
   {
      levelLoader.RestartLevel();
   }
   
   public GameLevel GetCurrentLevel()
   {
      return (GameLevel)levelSystem.Current;
   }
   public string GetCurrentLevelString()
   {
      return $"Level {levelSystem.VirtualLevelIndexForUI + 1}";
   }
   public int GetCurrentLevelNumber()
   {
      return levelSystem.VirtualLevelIndexForUI + 1;
   }
}

