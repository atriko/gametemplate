using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts
{
    [Serializable]
    public struct Notification
    {
        public string title;
        public string message;
        [Range(0,23)]
        public int hour;
        [Range(0,59)]
        public int minute;
    }
    [CreateAssetMenu(fileName = "NotificationSettings", menuName = "Game/NotificationSettings")]
    public class GameNotificationSettings : SingletonScriptableObject<GameNotificationSettings>
    {
        [Title("Do not forget the update the notification icon from EssentialKit",TitleAlignment = TitleAlignments.Centered)]
        public int notificationPopupLevelInterval;
        public Notification[] notifications;
        
    }
}
