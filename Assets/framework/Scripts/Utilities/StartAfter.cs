﻿using System;
using ScriptableObjectArchitecture;
using UniRx;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Utilities
{
    [AddComponentMenu(Architecture_Utility.BASE_MENU + "Game/" + nameof(StartAfter))]
    public class StartAfter : MonoBehaviour
    {
        [SerializeField] private float delay;
        [SerializeField] private UnityEvent unityEvent;
        private void Start()
        {
            Observable.Timer(TimeSpan.FromSeconds(delay)).Subscribe(l => unityEvent.Invoke());
        }
    }
}
