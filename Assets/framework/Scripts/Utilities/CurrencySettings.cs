using System.Collections;
using System.Collections.Generic;
using Framework.Game.Currency;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "CurrencySettings", menuName = "Game/CurrencySettings")]
public class CurrencySettings : SingletonScriptableObject<CurrencySettings>
{
    public int startMoney;
    public int extraLifePrice;
    
    [Button(ButtonSizes.Medium), GUIColor(0f, 1f, 0f)]
    public void ResetCurrency()
    {
        Currency.Instance.SetCurrency(startMoney);
    }
}
