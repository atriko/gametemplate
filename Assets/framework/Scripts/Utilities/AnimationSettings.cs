using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AnimationSettings", menuName = "Game/AnimationSettings")]
public class AnimationSettings : SingletonScriptableObject<AnimationSettings>
{
    public AnimationEventSpawn[] spawns;

}
