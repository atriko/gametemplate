using UnityEngine;

public class LogController
{
    [RuntimeInitializeOnLoadMethod]
    static void SetupLogging()
    {
        // Debug.Log("Setting up logs");

#if ENABLE_LOG || UNITY_EDITOR
        Debug.unityLogger.logEnabled = true;
#else
            Debug.unityLogger.logEnabled = false;
#endif
    }
}