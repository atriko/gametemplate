﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts
{
    [CreateAssetMenu(fileName = "GameConfigs", menuName = "Game/Configs")]
    public class GameConfig : SingletonScriptableObject<GameConfig>
    {
        public bool cpiMode;
    }
}