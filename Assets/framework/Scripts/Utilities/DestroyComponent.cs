using ScriptableObjectArchitecture;
using UnityEngine;

namespace DHFramework.Game.Utilities
{
    [AddComponentMenu(Architecture_Utility.BASE_MENU + "Game/" + nameof(DestroyComponent))]
    public class DestroyComponent : MonoBehaviour
    {
        [SerializeField] private Component componentToDestroy;

        public void Destroy()
        {
            Destroy(componentToDestroy);
        }
    }
}