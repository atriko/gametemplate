using ScriptableObjectArchitecture;
using UnityEngine;

namespace DHFramework.Game.AspectSetter
{
    [AddComponentMenu(Architecture_Utility.BASE_MENU + "Camera/" + nameof(CameraAspectSetter))]
    [RequireComponent(typeof(Camera))]
    public class CameraAspectSetter : MonoBehaviour
    {
        [SerializeField] private FloatVariable aspect;

        private Camera camera;

        private void OnValidate()
        {
            camera = GetComponent<Camera>();
        }

        private void Awake()
        {
            camera.aspect = aspect.Value;
        }
    }
}