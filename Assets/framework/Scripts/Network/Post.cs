﻿using System.Collections.Generic;
using System.Text;
using GameAssets.Scripts.Network;
using UniRx;

namespace DHFramework.Network
{
    internal class Post : NetworkOperation<string>
    {
        private string url;
        private string postData;
        private Dictionary<string, string> headers;

        public Post(string url, string postData, Dictionary<string, string> headers)
        {
            this.url = url;
            this.postData = postData;
            this.headers = headers;
        }

        public Post(string url)
        {
            this.url = url;

            postData = "0";
            headers = new Dictionary<string, string>();
        }

        public override void Process()
        {
            ObservableWWW.Post(url, Encoding.UTF8.GetBytes(postData), headers).Subscribe(RaiseOnNext, RaiseOnFail);
        }

        public override string ToString()
        {
            return $"HttpRequest: {GetType()} - {nameof(url)}: {url}";
        }
    }
}