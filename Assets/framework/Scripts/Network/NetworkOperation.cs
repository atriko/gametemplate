﻿using System;
using UniRx;

namespace GameAssets.Scripts.Network
{
    public abstract class NetworkOperation<T> : IObservable<T>, IDisposable
    {
        private Subject<T> requestSubject = new Subject<T>();

        public abstract void Process();

        public IDisposable Subscribe(IObserver<T> observer)
        {
            return requestSubject.Subscribe(observer);
        }

        protected void RaiseOnSuccess(T data)
        {
            requestSubject.OnNext(data);
            requestSubject.OnCompleted();
        }

        protected void RaiseOnFail(Exception e)
        {
            requestSubject.OnError(e);
            requestSubject.OnCompleted();
        }

        protected void RaiseOnNext(T data)
        {
            requestSubject.OnNext(data);
        }

        public void Dispose()
        {
            requestSubject?.Dispose();
        }
    }
}