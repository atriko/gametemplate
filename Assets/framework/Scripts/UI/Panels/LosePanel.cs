using System;
using Framework.Game.Currency;
using GameAssets.Scripts;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LosePanel : PanelBase
{
    public LosePanel()
    {
        Type = PanelType.Lose;
    }
    private void OnEnable()
    {
    }
    public void TryAgainClicked()
    {
        GameManager.Instance.LoseGame();
        LevelManager.Instance.RestartLevel();
    }
    
    public void BuyNewLifeClicked()
    {
    }


}
