using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InGamePanel : PanelBase
{
    [SerializeField] private Slider slider;
    [SerializeField] private TextMeshProUGUI levelText;
    [SerializeField] private TextMeshProUGUI movesText;

    private IDisposable gameStatusDisposable;
    private float fillPerCard;
    private int levelCardCount;
    private Tween fillTween;

    public InGamePanel()
    {
        Type = PanelType.InGame;
    }

    private void Start()
    {
        Initialize(100);
    }

    public void Initialize(int progressMaxElements)
    {
        levelText.text = LevelManager.Instance.GetCurrentLevelString();
        levelCardCount = progressMaxElements;
        fillPerCard = 100f / levelCardCount;
    }
    // private void UpdateSlider(int remainingCards)
    // {
    //     fillTween?.Kill();
    //     var currentSliderValue = slider.value;
    //     var targetSliderValue = (levelCardCount - remainingCards) * fillPerCard / 100;
    //     targetSliderValue = Remap(targetSliderValue, 0, 1, 0, 100);
    //     fillTween = DOVirtual.Float(currentSliderValue, targetSliderValue, 0.2f, delegate(float value) { slider.value = value; });
    // }
    private static float Remap(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
    public void ShowSettingsPopup()
    {
        UIManager.Instance.ShowPanelAdditive(PanelType.Settings);
    }
}