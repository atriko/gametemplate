using DG.Tweening;
using UnityEngine;

public class MainMenuPanel : PanelBase
{
    [SerializeField] private Transform selection;
    [SerializeField] private Transform[] selectionTargets;
    [SerializeField] private GameObject mainPanel;

    public MainMenuPanel()
    {
        Type = PanelType.Main;
    }

    public void ShowDaily()
    {
        mainPanel.SetActive(false);
        UIManager.Instance.HidePanel(PanelType.Collection);
        UIManager.Instance.ShowPanelAdditive(PanelType.Daily);
        
        selection.transform.DOLocalMoveX(selectionTargets[2].transform.localPosition.x, 0.1f);
    }

    public void ShowMain()
    {
        mainPanel.SetActive(true);
        UIManager.Instance.HidePanel(PanelType.Collection);
        UIManager.Instance.HidePanel(PanelType.Daily);
        
        selection.transform.DOLocalMoveX(selectionTargets[1].transform.localPosition.x, 0.1f);
    }

    public void ShowCollection()
    {
        mainPanel.SetActive(false);
        UIManager.Instance.HidePanel(PanelType.Daily);
        UIManager.Instance.ShowPanelAdditive(PanelType.Collection);

        selection.transform.DOLocalMoveX(selectionTargets[0].transform.localPosition.x, 0.1f);
    }

    public void OnClickPlay()
    {
        GameManager.Instance.StartGame();
        UIManager.Instance.StartFromMainMenu = false;
    }
}