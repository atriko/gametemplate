﻿using System;
using ScriptableObjectArchitecture;
using Sirenix.OdinInspector;
using UniRx;
using UnityEngine;

namespace GameAssets.Scripts.UI
{
    public class TutorialPanel : MonoBehaviour
    {
        [SerializeField] private float skipTutorialDuration = 1f;
        [SerializeField] private GameObject content;

        [Title("Events")] 
        [SerializeField] private GameEvent onGameStart;
        [SerializeField] private GameEvent onFirstTutorialShowed;

        private bool thisTutorialLevel = false;

        private void Awake()
        {
            thisTutorialLevel = false;
            content.SetActive(false);

            onFirstTutorialShowed.Subscribe(_ => thisTutorialLevel = true).AddTo(gameObject);
         
            onGameStart.Subscribe(_ =>
            {
                if (thisTutorialLevel)
                {
                    content.SetActive(true);
                    
                    Observable.Timer(TimeSpan.FromSeconds(skipTutorialDuration))
                        .Subscribe(l => content.SetActive(false)).AddTo(gameObject);
                }
            }).AddTo(gameObject);
        }
    }
}