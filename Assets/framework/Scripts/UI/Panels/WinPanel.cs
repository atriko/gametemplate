using System;
using TMPro;
using UnityEngine;

public class WinPanel : PanelBase
{
    public WinPanel()
    {
        Type = PanelType.Win;
    }

    private void OnEnable()
    {
        SoundManager.Instance.PlayWinSound();
    }

    public void NextClicked()
    {
        LevelManager.Instance.LoadNextLevel();
    }

}