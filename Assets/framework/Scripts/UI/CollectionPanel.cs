using System.Linq;
using Sirenix.Utilities;
using UnityEngine;

public class CollectionPanel : PanelBase
{
    public CollectionPanel()
    {
        Type = PanelType.Collection;
    }

    private void OnEnable()
    {
        UpdateCards();
    }
    private void UpdateCards()
    {
        
    }
    public void OnClickClose()
    {
        gameObject.SetActive(false);
    }
}