using System;
using System.Linq;
using DG.Tweening;
using GameAssets.Scripts;
using TMPro;
using UnityEngine;

[Serializable]
public class MultiplierPopupSettings
{
    public int targetStreak;
    public float size;
    public Color color;
}
public class MultiplierPopup : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI multiplierText;
    private void OnEnable()
    {
        // var settings = GameConfig.Instance.popups.First(settings =>  ComboController.Instance.VisualStreak < settings.targetStreak);
        //
        // multiplierText.text = $"x{ComboController.Instance.VisualStreak}";
        // multiplierText.color = settings.color;
        // transform.localScale = Vector3.zero;
        //
        // transform.DOScale(Vector3.one * settings.size, 0.3f).OnComplete(delegate
        // {
        //     transform.DOMoveY(transform.position.y + 50f, 1f).OnComplete(delegate
        //     {
        //         transform.DOScale(Vector3.zero, 0.3f).OnComplete(() => gameObject.SetActive(false));
        //     });
        // });
    }
}
