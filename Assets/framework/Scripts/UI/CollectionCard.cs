using System;
using TMPro;
using UnityEngine;

public class CollectionCard : MonoBehaviour
{
    [SerializeField] private GameObject cardFront;
    [SerializeField] private GameObject shine;
    [SerializeField] private int unlockLevel;
    [SerializeField] private TextMeshProUGUI levelText;
    public int UnlockLevel => unlockLevel;

    private void OnEnable()
    {
        levelText.text = $"Level {unlockLevel}";
    }

    public void Open()
    {
        if (LevelManager.Instance.GetCurrentLevelNumber() != unlockLevel)
        {
            shine.SetActive(false);
        }
        cardFront.SetActive(true);
    }
}