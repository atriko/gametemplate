﻿using Framework.Game.Currency;
using Sirenix.OdinInspector;
using TMPro;
using UniRx;
using UnityEngine;

namespace GameAssets.Scripts.UI
{
    public class CurrencyPopup : MonoBehaviour
    {
        [SerializeField] private Currency currency;
        
        [Title("Texts")]
        [SerializeField] private TextMeshProUGUI currencyText;
        
        private void Awake()
        {
            UpdateCoinText();
            currency.ObserveEveryValueChanged(_ => _.CurrentCurrency).Subscribe(_ => UpdateCoinText()).AddTo(gameObject);
        }

        private void UpdateCoinText() => currencyText.text = $"{currency.CurrentCurrency}";
    }
}