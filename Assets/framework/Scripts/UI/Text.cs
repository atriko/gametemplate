using ScriptableObjectArchitecture;
using UnityEngine;

namespace DHFramework.Game.UI
{
    [AddComponentMenu(Architecture_Utility.BASE_MENU + "UI/" + nameof(Text))]
    public class Text : MonoBehaviour
    {
        [SerializeField] private UnityEngine.UI.Text text;

        public void SetText(int number)
        {
            this.text.text = number.ToString();
        }

        public void SetText(string text)
        {
            this.text.text = text;
        }
    }
}