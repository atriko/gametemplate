﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

namespace AppAssets.Scripts
{
    public class CurrencyMovementHandler : MonoBehaviour
    {
        public static CurrencyMovementHandler Instance { get; private set; }

        [Title("UI references")] 
        [SerializeField] private GameObject animatedCoinPrefab;
        [SerializeField] private RectTransform target;

        [Title("Available coins : (coins to pool)")] 
        [SerializeField] private int maxCoins = 100;

        [Title("Animation settings")] 
        [SerializeField] [Range(0f, 1f)] private float minAnimDuration = 0.65f;
        [SerializeField] [Range(0f, 1f)] private float maxAnimDuration = 0.65f;
        [SerializeField] private Ease easeType = Ease.InBack;
        [SerializeField] private float spread = 50f;

        private Queue<GameObject> coinsQueue = new Queue<GameObject>();
        private Vector3 targetPosition;

        private void Awake()
        {
            Instance = this;
            
            targetPosition = target.position;
            PrepareCoins();
        }

        private void PrepareCoins()
        {
            for (int i = 0; i < maxCoins; i++) 
            {
                GameObject coin = Instantiate (animatedCoinPrefab, transform, true);
                coin.SetActive (false);
                coinsQueue.Enqueue (coin);
            }
        }

        private void Animate(Vector3 collectedCoinPosition, int amount)
        {
            for (int i = 0; i < amount; i++) 
            {
                if (coinsQueue.Count > 0) 
                {
                    GameObject coinModel = coinsQueue.Dequeue();
                    coinModel.SetActive (true);

                    coinModel.transform.position = collectedCoinPosition + new Vector3 (Random.Range (-spread, spread), 0f, 0f);

                    float duration = Random.Range (minAnimDuration, maxAnimDuration);
                    coinModel.transform.DOMove (targetPosition, duration).SetEase (easeType).OnComplete (() => 
                        {
                            coinModel.SetActive (false);
                            coinsQueue.Enqueue (coinModel);
                        });
                }
            }
        }

        public void AddCoins(Vector3 collectedCoinPosition, int amount)
        {
            Animate(collectedCoinPosition, amount);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                AddCoins(Vector3.zero, 10);
            }
        }
    }
}