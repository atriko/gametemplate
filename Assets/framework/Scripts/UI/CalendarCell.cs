
using GameAssets.Scripts;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CalendarCell : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI dayText;
    [SerializeField] private Image background;
    [SerializeField] private GameObject check;
    
    public void SetPastDay(int day)
    {
        dayText.text = day.ToString();
    }
    public void SetCurrentDay(int day)
    {
        dayText.text = day.ToString();
    }
    public void SetFutureDay(int day)
    {
        dayText.text = day.ToString();
    }
    public void SetAsComplete()
    {
        check.gameObject.SetActive(true);
        dayText.gameObject.SetActive(false);
    }
}
