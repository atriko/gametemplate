using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class DailyPanel : PanelBase
{
    [SerializeField] private TextMeshProUGUI dailyPlayButtonText;
    private List<CalendarCell> calendarButtons;
    
    public DailyPanel()
    {
        Type = PanelType.Daily;
    }
    public void OnClickDaily()
    {
        // LevelGenerator.Instance.HandleDailyLevel();
    }
    private void Awake()
    {
        calendarButtons = GetComponentsInChildren<CalendarCell>().ToList();
        SetCalendarNumbers();
    }
    private void SetCalendarNumbers()
    {
        var today = DateTime.Now.Day;
        dailyPlayButtonText.text = $"PLAY {AddOrdinal(today)}";
        for (int i = 0; i < 30; i++)
        {
            if (i + 1 < today)
            {
                calendarButtons[i].SetPastDay(i + 1);
            }
            else if (i + 1 == today)
            {
                calendarButtons[i].SetCurrentDay(i + 1);
            }
            else
            {
                calendarButtons[i].SetFutureDay(i + 1);
            }

            if (IsDailyComplete(i + 1))
            {
                calendarButtons[i].SetAsComplete();
            }
        }
    }
    private bool IsDailyComplete(int i)
    {
        return PlayerPrefs.GetInt($"{DateTime.Now.Month}{i}", 0) == 1;
    }

    private string AddOrdinal(int num)
    {
        if( num <= 0 ) return num.ToString();

        switch(num % 100)
        {
            case 11:
            case 12:
            case 13:
                return num + "th";
        }
    
        switch(num % 10)
        {
            case 1:
                return num + "st";
            case 2:
                return num + "nd";
            case 3:
                return num + "rd";
            default:
                return num + "th";
        }
    }
}
