using System;
using UniRx;
using UnityEngine;

namespace AppAssets.Scripts
{
    public class SafeArea : MonoBehaviour
    {
        private RectTransform panel;
        private Rect lastSafeArea = new Rect(0, 0, 0, 0);
        private IDisposable disposable;

        private void Awake()
        {
            panel = GetComponent<RectTransform>();
        }

        public void Start()
        {
            disposable?.Dispose();
            disposable = Observable.EveryUpdate().Subscribe(_ => Refresh()).AddTo(gameObject);
        }

        public void Stop()
        {
            disposable?.Dispose();
        }

        private void Refresh()
        {
            Rect safeArea = GetSafeArea();
            if (safeArea != lastSafeArea) ApplySafeArea(safeArea);
        }

        private Rect GetSafeArea() => Screen.safeArea;

        private void ApplySafeArea(Rect r)
        {
            lastSafeArea = r;
            Vector2 anchorMin = r.position;
            Vector2 anchorMax = r.position + r.size;
            anchorMin.x /= Screen.width;
            anchorMin.y /= Screen.height;
            anchorMax.x /= Screen.width;
            anchorMax.y /= Screen.height;
            panel.anchorMin = anchorMin;
            panel.anchorMax = anchorMax;
        }
    }
}