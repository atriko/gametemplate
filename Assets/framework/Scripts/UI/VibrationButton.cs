using GameAssets.Scripts;
using UnityEngine;

public class VibrationButton : MonoBehaviour
{
    [SerializeField] private GameObject muted;
    [SerializeField] private GameObject unmuted;

    private void Awake()
    {
        UpdateIcon();
    }

    private void UpdateIcon()
    {
        muted.SetActive(!HapticManager.Instance.HapticEnabled);
        unmuted.SetActive(HapticManager.Instance.HapticEnabled);
    }

    public void OnClick()
    {
        HapticManager.Instance.UpdateHaptic(!HapticManager.Instance.HapticEnabled);
        UpdateIcon();
    }
}
