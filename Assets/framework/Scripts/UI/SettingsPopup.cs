using System;

public class SettingsPopup : PanelBase
{
    public SettingsPopup()
    {
        Type = PanelType.Settings;
    }
    
    public void HomeClicked()
    {
        LevelManager.Instance.RestartLevel();
    }
}
