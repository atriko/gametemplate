﻿using GameAssets.Scripts;
using ScriptableObjectArchitecture;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Framework.Game.Currency
{
    [CreateAssetMenu(menuName = "Framework/Game/" + nameof(Currency))]
    public class Currency : SingletonScriptableObject<Currency>
    {
        private const string currencyKey = "currency";

        [SerializeField] private IntGameEvent onCurrencyChanged;
        [SerializeField] private GameEventBase onCurrencyReset;
        [SerializeField] private float multiplier = 1;

        public float Multiplier
        {
            get => multiplier;
            set => multiplier = value;
        }

        private void OnEnable()
        {
            currentCurrency = PlayerPrefs.GetInt(currencyKey, CurrencySettings.Instance.startMoney);
        }

        private int currentCurrency;

        public int CurrentCurrency
        {
            get { return currentCurrency; }
        }

        public void AddCurrency(int incrementAmount)
        {
            int change = (int)(incrementAmount * multiplier);
            onCurrencyChanged.Raise(change);
            SetCurrency(currentCurrency + change);
        }
        public void SetCurrency(int currency)
        {
            currentCurrency = currency;
            onCurrencyChanged.Raise(currentCurrency);
            PlayerPrefs.SetInt(currencyKey, currentCurrency);
        }

        [Button(ButtonSizes.Medium), GUIColor(0f, 1f, 0f)]
        public void ResetCurrency()
        {
            SetCurrency(0);
            onCurrencyReset.Raise();
        }
    }
}