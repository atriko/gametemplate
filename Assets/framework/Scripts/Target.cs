using DG.Tweening;
using TMPro;
using UnityEngine;

public class Target : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI countText;
    [SerializeField] private GameObject checkMark;
    [SerializeField] private GameObject icon;
    private int currentCount;
    private bool isCompleted;
    private string targetType;
    public bool IsCompleted => isCompleted;
    public string TargetType => targetType;
    private Tween punchTween;

    public void Set(string type, int count)
    {
        targetType = type;
        currentCount = count;
        UpdateText();
    }

    private void UpdateText()
    {
        punchTween?.Kill(true);
        punchTween = countText.transform.DOPunchScale(Vector3.one * 0.1f, 0.2f);
        countText.text = $"x{currentCount}";
    }

    public void Hit()
    {
        if (isCompleted)
            return;
        currentCount--;
        if (currentCount <= 0)
        {
            isCompleted = true;

            icon.SetActive(false);
            countText.gameObject.SetActive(false);

            checkMark.SetActive(true);
            checkMark.transform.DOPunchScale(Vector3.one * 0.5f, 1.2f,0,0).OnComplete(() => gameObject.SetActive(false));
        }

        UpdateText();
    }
}