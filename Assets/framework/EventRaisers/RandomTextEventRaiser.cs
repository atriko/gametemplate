﻿using ScriptableObjectArchitecture;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace EventRaisers.UnityCallbacks
{
    public class RandomTextEventRaiser : MonoBehaviour
    {
        [Header("Parameters")] 
        [SerializeField]
        private string[] randomTexts;

        [SerializeField]
        private TextMeshProUGUI textMeshField;
        [SerializeField]
        private Text unityUiTextField;
        
        
        [SerializeField] private bool raiseOnce;
        
        
        [SerializeField] private GameEvent gameEvent;
        private int eventRepeatCount;
        
        private void OnEnable()
        {
            gameEvent.AddListener(OnEventRaised);
        }

        private void OnDisable()
        {
            gameEvent.RemoveListener(OnEventRaised);
        }

        private void OnEventRaised()
        {
            if (--eventRepeatCount <= 0)
            {
                int randomIndex = Random.Range(0, randomTexts.Length - 1);
                
                if (textMeshField != null)
                {
                    textMeshField.text = randomTexts[randomIndex];
                }

                if (unityUiTextField != null)
                {
                    unityUiTextField.text = randomTexts[randomIndex];
                }
                
                if (raiseOnce)
                {
                    gameEvent.RemoveListener(OnEventRaised);
                    Destroy(gameObject);
                }
            }
        }

    }
}
