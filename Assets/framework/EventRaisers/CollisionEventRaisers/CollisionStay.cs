using ScriptableObjectArchitecture;
using UnityEngine;

namespace DHFramework.EventRaisers.CollisionEventRaisers
{
    [AddComponentMenu(Architecture_Utility.RAISER + nameof(CollisionStay))]
    public class CollisionStay : CollisionEventRaiser
    {
        private void OnCollisionStay(Collision other)
        {
            RaiseIfTagMatches(other);
        }
    }
}