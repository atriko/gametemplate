using System;
using ScriptableObjectArchitecture;
using Sirenix.OdinInspector;
using UniRx;
using UnityEngine;
using UnityEngine.Events;

namespace DHFramework.EventRaisers.CollisionEventRaisers
{
    [AddComponentMenu(Architecture_Utility.RAISER + nameof(CollisionEventRaiser))]
    public abstract class CollisionEventRaiser : EventRaiser
    {
        [GUIColor("GetColor")] [SerializeField]
        private bool isEnable = true;

        [SerializeField] private float startDuration = 0f;

        [Serializable]
        public class Event : UnityEvent<Collision>
        {
        }

        [SerializeField] protected Event onHappened;
        [SerializeField] protected string tag = "Untagged";

        public void EnableEvent() => isEnable = true;
        public void DisableEvent() => isEnable = false;

        protected void RaiseIfTagMatches(Collision c)
        {
            if (isEnable && c.gameObject.CompareTag(tag))
            {
                Debug.Log($"[CollisionEventRaiser]: {GetType()}", gameObject);
                Observable.Timer(TimeSpan.FromSeconds(startDuration)).Subscribe(_ => onHappened.Invoke(c));
            }
        }

        private Color GetColor()
        {
            return this.isEnable ? Color.green : Color.red;
        }
    }
}