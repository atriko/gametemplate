using ScriptableObjectArchitecture;
using UnityEngine;

namespace DHFramework.EventRaisers.CollisionEventRaisers
{
    [AddComponentMenu(Architecture_Utility.RAISER + nameof(CollisionExit))]
    public class CollisionExit : CollisionEventRaiser
    {
        private void OnCollisionExit(Collision other)
        {
            RaiseIfTagMatches(other);
        }
    }
}