using ScriptableObjectArchitecture;
using UnityEngine;

namespace DHFramework.EventRaisers.CollisionEventRaisers
{
    [AddComponentMenu(Architecture_Utility.RAISER + nameof(CollisionEnter))]
    public class CollisionEnter : CollisionEventRaiser
    {
        private void OnCollisionEnter(Collision other)
        {
            RaiseIfTagMatches(other);
        }
    }
}