using ScriptableObjectArchitecture;
using UnityEngine;

namespace DHFramework.EventRaisers.TriggerEventRaisers
{
    [AddComponentMenu(Architecture_Utility.RAISER + nameof(TriggerExit))]
    public class TriggerExit : TriggerEventRaiser
    {
        private void OnTriggerExit(Collider other)
        {
            RaiseIfTagMatches(other);
        }
    }
}