using ScriptableObjectArchitecture;
using UnityEngine;

namespace DHFramework.EventRaisers.TriggerEventRaisers
{
    [AddComponentMenu(Architecture_Utility.RAISER + nameof(TriggerStay))]
    public class TriggerStay : TriggerEventRaiser
    {
        private void OnTriggerStay(Collider other)
        {
            RaiseIfTagMatches(other);
        }
    }
}