using System;
using ScriptableObjectArchitecture;
using Sirenix.OdinInspector;
using UniRx;
using UnityEngine;
using UnityEngine.Events;

namespace DHFramework.EventRaisers.TriggerEventRaisers
{
    [AddComponentMenu(Architecture_Utility.RAISER + nameof(TriggerEventRaiser))]
    public abstract class TriggerEventRaiser : EventRaiser
    {
        [GUIColor("GetColor")] [SerializeField]
        private bool isEnable = true;

        [SerializeField] private float startDuration = 0f;

        [Serializable]
        public class Event : UnityEvent<Collider>
        {
        }

        [SerializeField] protected Event onHappened;
        [SerializeField] protected string tag = "Untagged";

        public void EnableEvent() => isEnable = true;
        public void DisableEvent() => isEnable = false;

        public void AddListener(UnityAction<Collider> listener)
        {
            onHappened.AddListener(listener);
        }

        public void RemoveListener(UnityAction<Collider> listener)
        {
            onHappened.RemoveListener(listener);
        }

        protected void RaiseIfTagMatches(Collider other)
        {
            if (isEnable && other.CompareTag(tag))
            {
                Debug.Log($"[TriggerEventRaiser]: {GetType()}", gameObject);
                Observable.Timer(TimeSpan.FromSeconds(startDuration)).Subscribe(_ => onHappened.Invoke(other));
            }
        }

        private Color GetColor()
        {
            return this.isEnable ? Color.green : Color.red;
        }
    }
}