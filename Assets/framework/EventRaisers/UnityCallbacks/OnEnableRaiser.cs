﻿using ScriptableObjectArchitecture;
using UnityEngine;
using UnityEngine.Events;

namespace DHFramework.EventRaisers.UnityCallbacks
{
    [AddComponentMenu(Architecture_Utility.RAISER + nameof(OnEnableRaiser))]
    public class OnEnableRaiser : MonoBehaviour
    {
        [SerializeField] private UnityEvent onHappened;

        private void OnEnable()
        {
            onHappened.Invoke();
        }
    }
}