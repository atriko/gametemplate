using ScriptableObjectArchitecture;
using UnityEngine;
using UnityEngine.Events;

namespace DHFramework.EventRaisers.UnityCallbacks
{
    [AddComponentMenu(Architecture_Utility.RAISER + nameof(StartRaiser))]
    public class StartRaiser : MonoBehaviour
    {
        [SerializeField] private UnityEvent onHappened;

        private void Start()
        {
            onHappened.Invoke();
        }
    }
}