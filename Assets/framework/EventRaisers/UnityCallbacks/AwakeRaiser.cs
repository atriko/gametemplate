using ScriptableObjectArchitecture;
using UnityEngine;
using UnityEngine.Events;

namespace DHFramework.EventRaisers.UnityCallbacks
{
    [AddComponentMenu(Architecture_Utility.RAISER + nameof(AwakeRaiser))]
    public class AwakeRaiser : MonoBehaviour
    {
        [SerializeField] private UnityEvent onHappened;

        private void Awake()
        {
            onHappened.Invoke();
        }
    }
}