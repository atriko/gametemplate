﻿using ScriptableObjectArchitecture;
using UnityEngine;
using UnityEngine.Events;

namespace DHFramework.EventRaisers
{
    [AddComponentMenu(Architecture_Utility.RAISER + nameof(GameEventRaiser))]
    public class GameEventRaiser : EventRaiser
    {
        [SerializeField]private UnityEvent onHappened;

        public void Raise()
        {
            onHappened?.Invoke();
        }
    }
}
