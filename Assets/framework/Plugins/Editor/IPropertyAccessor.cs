﻿namespace Plugins.Editor
{
    public interface IPropertyAccessor
    {
        object GetValue(object source);
    }
}