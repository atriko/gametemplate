﻿using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [CreateAssetMenu(
        fileName = "ObjectVariable.asset",
        menuName = Architecture_Utility.VARIABLE_SUBMENU + "Object",
        order = Architecture_Utility.ASSET_MENU_ORDER_COLLECTIONS + 1)]
    public class ObjectVariable : BaseVariable<Object>
    {
    } 
}