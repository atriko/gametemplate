﻿using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [CreateAssetMenu(
        fileName = "CharVariable.asset",
        menuName = Architecture_Utility.ADVANCED_VARIABLE_SUBMENU + "char",
        order = Architecture_Utility.ASSET_MENU_ORDER_COLLECTIONS + 7)]
    public sealed class CharVariable : BaseVariable<char>
    {
    } 
}