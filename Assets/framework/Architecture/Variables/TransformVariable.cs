using UnityEngine;

namespace ScriptableObjectArchitecture
{
	[CreateAssetMenu(
	    fileName = "TransformVariable.asset")]
	public class TransformVariable : BaseVariable<Transform>
	{
	}
}