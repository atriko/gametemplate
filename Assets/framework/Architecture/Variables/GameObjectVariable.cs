using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [CreateAssetMenu(
        fileName = "GameObjectVariable.asset",
        menuName = Architecture_Utility.VARIABLE_SUBMENU + "GameObject",
        order = Architecture_Utility.ASSET_MENU_ORDER_COLLECTIONS + 0)]
    public sealed class GameObjectVariable : BaseVariable<GameObject>
    {
    } 
}