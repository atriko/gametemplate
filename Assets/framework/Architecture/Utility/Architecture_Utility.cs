﻿namespace ScriptableObjectArchitecture
{
    public static class Architecture_Utility
    {
        public const int ASSET_MENU_ORDER_VARIABLES = 121;
        public const int ASSET_MENU_ORDER_EVENTS = 122;
        public const int ASSET_MENU_ORDER_COLLECTIONS = 123;

        public const string BASE_MENU = "Framework/";
        public const string RAISER = BASE_MENU + "Raiser/";
        public const string EXTRAS = BASE_MENU + "Extras/";

        public const string VARIABLE_SUBMENU = BASE_MENU + "Variables/";
        public const string COLLECTION_SUBMENU = BASE_MENU + "Collections/";
        public const string GAME_EVENT = BASE_MENU + "Game Events/";

        public const string ADVANCED_GAME_EVENT = BASE_MENU + GAME_EVENT;
        public const string ADVANCED_VARIABLE_SUBMENU = BASE_MENU + VARIABLE_SUBMENU;
        public const string ADVANCED_VARIABLE_COLLECTION = BASE_MENU + COLLECTION_SUBMENU;

        // Add Component Menus
        public const string ADD_COMPONENT_ROOT_MENU = BASE_MENU;
        public const string EVENT_LISTENER_SUBMENU = ADD_COMPONENT_ROOT_MENU + "Event Listeners/";
    }
}