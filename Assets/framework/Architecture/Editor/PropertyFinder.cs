﻿using System;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace DHFramework.Architecture.InspectorFinder
{
//    [CustomEditor(typeof(Transform)), CanEditMultipleObjects,ExecuteInEditMode]
    public class PropertyFinder : UnityEditor.Editor
    {
        private string search = String.Empty;
        private bool currentComponentIsActive = false;

        private void OnValidate()
        {
            search = string.Empty;
            ShowAllComponent();
        }

        public override void OnInspectorGUI()
        {
            GUIStyle guiStyle = GUI.skin.GetStyle("Label");
            guiStyle.alignment = TextAnchor.UpperLeft;

            Transform transform = (Transform) target;
            DrawTransformInspector(transform);

            GUILayout.Space(5);

            guiStyle.alignment = TextAnchor.UpperCenter;
            GUILayout.Label("Property Find", guiStyle);
            search = GUILayout.TextField(search);

            GUI.color = Color.green;
            if (GUILayout.Button("Find", GUILayout.Height(25)))
            {
                FindProperty();
            }

            if (GUILayout.Button("Show All Components", GUILayout.Height(25)))
            {
                ShowAllComponent();
            }
        }

        private void ShowAllComponent()
        {
            Transform gameObject = (Transform) target;
            Component[] components = gameObject.GetComponents(typeof(Component));
            foreach (Component component in components)
            {
                SetComponentVisible(component);
            }
        }

        private void FindProperty()
        {
            Transform gameObject = (Transform) target;
            Component[] components = gameObject.GetComponents(typeof(Component));
            foreach (Component component in components)
            {
                if (component == this) continue;

                currentComponentIsActive = false;

                SerializedObject serializedObject = new SerializedObject(component);

                SerializedProperty property = serializedObject.GetIterator();
                if (property.NextVisible(true))
                {
                    do
                    {
                        property = serializedObject.FindProperty(property.name);
                        if (property != null)
                        {
                            Object obj = null;
                            if (property.propertyType == SerializedPropertyType.ObjectReference)
                            {
                                obj = property.objectReferenceValue;
                            }

                            if (obj != null)
                            {
                                bool objectReferenceValue = obj != null && obj.ToString().Contains(search);
                                bool propertyQuery = property.name.Contains(search);
                                if (propertyQuery || objectReferenceValue) currentComponentIsActive = true;
                            }
                        }
                    } while (property != null && property.NextVisible(false));
                }

                serializedObject.ApplyModifiedProperties();

                SetComponentVisible(component, currentComponentIsActive);
            }
        }

        private void SetComponentVisible(Component component, bool value = true)
        {
            if (component is Transform) component.hideFlags = HideFlags.None;
            else component.hideFlags = value ? HideFlags.None : HideFlags.HideInInspector;
            EditorUtility.SetDirty(component);
        }

        public void DrawTransformInspector(Transform transform)
        {
            EditorGUIUtility.labelWidth = 25;
            EditorGUIUtility.fieldWidth = 50;

            EditorGUI.indentLevel = 0;
            GUILayout.Label("Position");
            Vector3 position = EditorGUILayout.Vector3Field(String.Empty, transform.localPosition);
            GUILayout.Label("Rotation");
            Vector3 eulerAngles = EditorGUILayout.Vector3Field(String.Empty, transform.localEulerAngles);
            GUILayout.Label("Scale");
            Vector3 scale = EditorGUILayout.Vector3Field(String.Empty, transform.localScale);

            EditorGUIUtility.labelWidth = 0;
            EditorGUIUtility.fieldWidth = 0;

            if (GUI.changed)
            {
                Undo.RecordObject(transform, "Transform Change");

                transform.localPosition = FixIfNaN(position);
                transform.localEulerAngles = FixIfNaN(eulerAngles);
                transform.localScale = FixIfNaN(scale);
            }
        }

        private Vector3 FixIfNaN(Vector3 vector)
        {
            if (float.IsNaN(vector.x))
            {
                vector.x = 0.0f;
            }

            if (float.IsNaN(vector.y))
            {
                vector.y = 0.0f;
            }

            if (float.IsNaN(vector.z))
            {
                vector.z = 0.0f;
            }

            return vector;
        }
    }
}