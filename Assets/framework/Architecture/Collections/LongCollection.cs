using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [CreateAssetMenu(
        fileName = "LongCollection.asset",
        menuName = Architecture_Utility.ADVANCED_VARIABLE_COLLECTION + "long",
        order = Architecture_Utility.ASSET_MENU_ORDER_COLLECTIONS + 9)]
    public class LongCollection : Collection<long>
    {
    } 
}