using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [CreateAssetMenu(
        fileName = "ULongCollection.asset",
        menuName = Architecture_Utility.ADVANCED_VARIABLE_COLLECTION + "ulong",
        order = Architecture_Utility.ASSET_MENU_ORDER_COLLECTIONS + 17)]
    public class ULongCollection : Collection<ulong>
    {
    } 
}