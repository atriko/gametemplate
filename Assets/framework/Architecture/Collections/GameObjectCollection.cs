using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [CreateAssetMenu(
        fileName = "GameObjectCollection.asset",
        menuName = Architecture_Utility.COLLECTION_SUBMENU + "GameObject",
        order = Architecture_Utility.ASSET_MENU_ORDER_COLLECTIONS + 0)]
    public class GameObjectCollection : Collection<GameObject>
    {
    } 
}
