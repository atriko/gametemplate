using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [CreateAssetMenu(
        fileName = "UIntCollection.asset",
        menuName = Architecture_Utility.ADVANCED_VARIABLE_COLLECTION + "uint",
        order = Architecture_Utility.ASSET_MENU_ORDER_COLLECTIONS + 16)]
    public class UIntCollection : Collection<uint>
    {
    } 
}