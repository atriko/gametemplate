using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [CreateAssetMenu(
        fileName = "CharCollection.asset",
        menuName = Architecture_Utility.ADVANCED_VARIABLE_COLLECTION + "char",
        order = Architecture_Utility.ASSET_MENU_ORDER_COLLECTIONS + 7)]
    public class CharCollection : Collection<char>
    {
    } 
}