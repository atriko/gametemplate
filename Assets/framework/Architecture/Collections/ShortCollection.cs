using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [CreateAssetMenu(
        fileName = "ShortCollection.asset",
        menuName = Architecture_Utility.ADVANCED_VARIABLE_COLLECTION + "short",
        order = Architecture_Utility.ASSET_MENU_ORDER_COLLECTIONS + 14)]
    public class ShortCollection : Collection<short>
    {
    } 
}