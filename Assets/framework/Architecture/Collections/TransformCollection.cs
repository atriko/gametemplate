using UnityEngine;

namespace ScriptableObjectArchitecture
{
	[CreateAssetMenu(
	    fileName = "TransformCollection.asset")]
	public class TransformCollection : Collection<Transform>
	{
	}
}