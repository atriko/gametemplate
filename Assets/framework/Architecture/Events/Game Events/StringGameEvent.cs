﻿using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [System.Serializable]
    [CreateAssetMenu(
        fileName = "StringGameEvent.asset",
        menuName = Architecture_Utility.GAME_EVENT + "string",
        order = Architecture_Utility.ASSET_MENU_ORDER_EVENTS + 2)]
    public sealed class StringGameEvent : GameEventBase<string>
    {
    } 
}