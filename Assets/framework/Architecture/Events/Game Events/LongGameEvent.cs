﻿using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [System.Serializable]
    [CreateAssetMenu(
        fileName = "LongGameEvent.asset",
        menuName = Architecture_Utility.ADVANCED_GAME_EVENT + "long",
        order = Architecture_Utility.ASSET_MENU_ORDER_EVENTS + 9)]
    public sealed class LongGameEvent : GameEventBase<long>
    {
    } 
}