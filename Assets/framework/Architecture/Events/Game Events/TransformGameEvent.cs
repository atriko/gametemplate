using UnityEngine;

namespace ScriptableObjectArchitecture
{
	[System.Serializable]
	[CreateAssetMenu(
	    fileName = "TransformGameEvent.asset")]
	public sealed class TransformGameEvent : GameEventBase<Transform>
	{
	}
}