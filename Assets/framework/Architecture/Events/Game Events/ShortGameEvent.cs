﻿using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [System.Serializable]
    [CreateAssetMenu(
        fileName = "ShortGameEvent.asset",
        menuName = Architecture_Utility.ADVANCED_GAME_EVENT + "short",
        order = Architecture_Utility.ASSET_MENU_ORDER_EVENTS + 14)]
    public sealed class ShortGameEvent : GameEventBase<short>
    {
    } 
}