﻿using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [System.Serializable]
    [CreateAssetMenu(
        fileName = "IntGameEvent.asset",
        menuName = Architecture_Utility.GAME_EVENT + "int",
        order = Architecture_Utility.ASSET_MENU_ORDER_EVENTS + 4)]
    public sealed class IntGameEvent : GameEventBase<int>
    {
    } 
}