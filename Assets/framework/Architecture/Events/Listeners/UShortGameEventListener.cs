﻿using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [AddComponentMenu(Architecture_Utility.EVENT_LISTENER_SUBMENU + "ushort Event Listener")]
    public sealed class UShortGameEventListener : BaseGameEventListener<ushort, UShortGameEvent, UShortUnityEvent>
    {
        [SerializeField] private UShortUnityEvent response;

        protected override void OnResponse(ushort value)
        {
            response.Invoke(value);
        }
    }
}