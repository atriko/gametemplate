﻿using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [AddComponentMenu(Architecture_Utility.EVENT_LISTENER_SUBMENU + "long Event Listener")]
    public sealed class LongGameEventListener : BaseGameEventListener<long, LongGameEvent, LongUnityEvent>
    {
        [SerializeField] private LongUnityEvent response;

        protected override void OnResponse(long value)
        {
            response.Invoke(value);
        }
    }
}