﻿using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [AddComponentMenu(Architecture_Utility.EVENT_LISTENER_SUBMENU + "bool Event Listener")]
    public sealed class BoolGameEventListener : BaseGameEventListener<bool, BoolGameEvent, BoolUnityEvent>
    {
        [SerializeField] private BoolUnityEvent response;

        protected override void OnResponse(bool value)
        {
            response.Invoke(value);
        }
    }
}