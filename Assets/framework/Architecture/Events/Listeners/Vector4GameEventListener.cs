using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [AddComponentMenu(Architecture_Utility.EVENT_LISTENER_SUBMENU + "Vector4 Event Listener")]
    public sealed class Vector4GameEventListener : BaseGameEventListener<Vector4, Vector4GameEvent, Vector4UnityEvent>
    {
        [SerializeField] private Vector4UnityEvent response;

        protected override void OnResponse(Vector4 value)
        {
            response.Invoke(value);
        }
    }
}