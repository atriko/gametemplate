﻿using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [AddComponentMenu(Architecture_Utility.EVENT_LISTENER_SUBMENU + "string Event Listener")]
    public sealed class StringGameEventListener : BaseGameEventListener<string, StringGameEvent, StringUnityEvent>
    {
        [SerializeField] private StringUnityEvent response;

        protected override void OnResponse(string value)
        {
            response.Invoke(value);
        }
    }
}