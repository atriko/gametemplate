﻿using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [AddComponentMenu(Architecture_Utility.EVENT_LISTENER_SUBMENU + "double Event Listener")]
    public sealed class DoubleGameEventListener : BaseGameEventListener<double, DoubleGameEvent, DoubleUnityEvent>
    {
        [SerializeField] private DoubleUnityEvent response;

        protected override void OnResponse(double value)
        {
            response.Invoke(value);
        }
    }
}