﻿using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [AddComponentMenu(Architecture_Utility.EVENT_LISTENER_SUBMENU + "sbyte Event Listener")]
    public sealed class SByteGameEventListener : BaseGameEventListener<sbyte, SByteGameEvent, SByteUnityEvent>
    {
        [SerializeField] private SByteUnityEvent response;

        protected override void OnResponse(sbyte value)
        {
            response.Invoke(value);
        }
    }
}