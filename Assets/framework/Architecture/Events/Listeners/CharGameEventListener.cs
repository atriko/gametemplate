﻿using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [AddComponentMenu(Architecture_Utility.EVENT_LISTENER_SUBMENU + "char Event Listener")]
    public sealed class CharGameEventListener : BaseGameEventListener<char, CharGameEvent, CharUnityEvent>
    {
        [SerializeField] private CharUnityEvent response;

        protected override void OnResponse(char value)
        {
            response.Invoke(value);
        }
    }
}