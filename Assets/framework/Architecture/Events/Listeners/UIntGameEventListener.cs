﻿using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [AddComponentMenu(Architecture_Utility.EVENT_LISTENER_SUBMENU + "uint Event Listener")]
    public sealed class UIntGameEventListener : BaseGameEventListener<uint, UIntGameEvent, UIntUnityEvent>
    {
        [SerializeField] private UIntUnityEvent response;

        protected override void OnResponse(uint value)
        {
            response.Invoke(value);
        }
    }
}