﻿using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [AddComponentMenu(Architecture_Utility.EVENT_LISTENER_SUBMENU + "ulong Event Listener")]
    public sealed class ULongGameEventListener : BaseGameEventListener<ulong, ULongGameEvent, ULongUnityEvent>
    {
        [SerializeField] private ULongUnityEvent response;

        protected override void OnResponse(ulong value)
        {
            response.Invoke(value);
        }
    }
}