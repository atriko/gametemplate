﻿using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [AddComponentMenu(Architecture_Utility.EVENT_LISTENER_SUBMENU + "float Event Listener")]
    public sealed class FloatGameEventListener : BaseGameEventListener<float, FloatGameEvent, FloatUnityEvent>
    {
        [SerializeField] private FloatUnityEvent response;

        protected override void OnResponse(float value)
        {
            response.Invoke(value);
        }
    }
}