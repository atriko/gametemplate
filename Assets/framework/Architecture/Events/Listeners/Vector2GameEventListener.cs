using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [AddComponentMenu(Architecture_Utility.EVENT_LISTENER_SUBMENU + "Vector2 Event Listener")]
    public sealed class Vector2GameEventListener : BaseGameEventListener<Vector2, Vector2GameEvent, Vector2UnityEvent>
    {
        [SerializeField] private Vector2UnityEvent response;

        protected override void OnResponse(Vector2 value)
        {
            response.Invoke(value);
        }
    }
}