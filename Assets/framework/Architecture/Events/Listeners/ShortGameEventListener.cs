﻿using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [AddComponentMenu(Architecture_Utility.EVENT_LISTENER_SUBMENU + "short Event Listener")]
    public sealed class ShortGameEventListener : BaseGameEventListener<short, ShortGameEvent, ShortUnityEvent>
    {
        [SerializeField] private ShortUnityEvent response;

        protected override void OnResponse(short value)
        {
            response.Invoke(value);
        }
    }
}