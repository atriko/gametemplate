using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [AddComponentMenu(Architecture_Utility.EVENT_LISTENER_SUBMENU + "Quaternion Event Listener")]
    public sealed class QuaternionGameEventListener : BaseGameEventListener<Quaternion, QuaternionGameEvent, QuaternionUnityEvent>
    {
        [SerializeField] private QuaternionUnityEvent response;

        protected override void OnResponse(Quaternion value)
        {
            response.Invoke(value);
        }
    }
}