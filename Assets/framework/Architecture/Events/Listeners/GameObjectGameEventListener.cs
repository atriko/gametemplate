using UnityEngine;

namespace ScriptableObjectArchitecture
{
    [AddComponentMenu(Architecture_Utility.EVENT_LISTENER_SUBMENU + "GameObject Event Listener")]
    public sealed class GameObjectGameEventListener : BaseGameEventListener<GameObject, GameObjectGameEvent, GameObjectUnityEvent>
    {
        [SerializeField] private GameObjectUnityEvent response;

        protected override void OnResponse(GameObject value)
        {
            response.Invoke(value);
        }
    }
}