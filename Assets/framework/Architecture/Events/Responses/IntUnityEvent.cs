﻿using System;
using UnityEngine.Events;

namespace ScriptableObjectArchitecture
{
    [Serializable]
    public class IntUnityEvent : UnityEvent<int>
    {
    }
}