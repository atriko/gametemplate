﻿#if UNITY_EDITOR
using UnityEditor;
using Sirenix.OdinInspector;
#endif
using Framework.Game.Currency;
using UnityEngine;

[CreateAssetMenu(menuName = "Framework/Analytics/TinySauceReporter")]
public class TinySauceReporter : ScriptableObject
{
    [SerializeField] private Currency currency;
    [SerializeField] private LevelSystem levels;

#if UNITY_EDITOR
    private const string gameanalytics = "GAMEANALYTICS";

    [Button]
    void EnableGameAnalyticsForIos()
    {
        string def = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS);
        if (!def.Contains(gameanalytics))
        {
            def += $";{gameanalytics};";
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS, def);
        }
    }

    [Button]
    void DisableGameAnalyticsForIos()
    {
        string def = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS);
        if (def.Contains(gameanalytics))
        {
            def = def.Replace(gameanalytics, string.Empty);
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS, def);
        }
    }

    [Button]
    void EnableGameAnalyticsForAndroid()
    {
        string def = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android);
        if (!def.Contains(gameanalytics))
        {
            def += $";{gameanalytics};";
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, def);
        }
    }

    [Button]
    void DisableGameAnalyticsForAndroid()
    {
        string def = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android);
        if (def.Contains(gameanalytics))
        {
            def = def.Replace(gameanalytics, string.Empty);
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, def);
        }
    }
#endif


    public void OnGameStart()
    {
#if GAMEANALYTICS
        TinySauce.OnGameStarted("Level" + levels.VirtualLevelIndexForUI);
#endif
    }

    public void OnGameLost()
    {
#if GAMEANALYTICS
        TinySauce.OnGameFinished(false, currency.CurrentCurrency, "Level" + levels.VirtualLevelIndexForUI);
#endif
    }

    public void OnGameWon()
    {
#if GAMEANALYTICS
        TinySauce.OnGameFinished(true, currency.CurrentCurrency, "Level" + levels.VirtualLevelIndexForUI);
#endif
    }
}