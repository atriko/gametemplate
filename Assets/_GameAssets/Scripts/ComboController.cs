using GameAssets.Scripts;
using UnityEngine;

public class ComboController : MonoSingleton<ComboController>
{
    private int visualStreak;
    public int VisualStreak => visualStreak;

    public void IncreaseCombo(Vector3 position)
    {
        visualStreak++;
        if (visualStreak > 1)
        {
            // UIManager.Instance.ShowMultiplierPopup(position);
        }
        if (visualStreak % 3 == 0)
        {
            // ComplimentManager.Instance.ShowCompliment();
        }
    }
    public void LoseCombo()
    {
        visualStreak = 0;
    }
    public float GetPitch()
    {
        return MusicPlayer.Instance.basePitch + Mathf.Clamp(visualStreak,1,MusicPlayer.Instance.maxPitchStreak) * MusicPlayer.Instance.pitchPerLevel;
    }
}