using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : MonoBehaviour
{
    [SerializeField] private GameObject active;
    [SerializeField] private GameObject inactive;

    public void Active()
    {
        active.SetActive(true);
        inactive.SetActive(false);
    }

    public void Inactive()
    {
        active.SetActive(false);
        inactive.SetActive(true);
    }
}
