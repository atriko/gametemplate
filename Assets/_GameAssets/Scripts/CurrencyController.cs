using DG.Tweening;
using Framework.Game.Currency;
using TMPro;
using UnityEngine;

public class CurrencyController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI currencyText;

    private Tween punchTween;

    private void OnEnable()
    {
        UpdateCurrencyUI();
        EventManager.Instance.SubscribeEvent(GameEventType.CurrencyChanged, UpdateCurrencyUI);
    }

    private void UpdateCurrencyUI()
    {
        punchTween?.Kill(true);
        punchTween = currencyText.transform.DOPunchScale(Vector3.one * 0.1f, 0.2f);
        currencyText.text = Currency.Instance.CurrentCurrency.ToString();
    }
}