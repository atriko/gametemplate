using System;
using Sirenix.OdinInspector;
using UnityEngine;

public class UIDebugger : MonoBehaviour
{
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            UIManager.Instance.ShowPanel(PanelType.Win);
        }
        else if (Input.GetKeyDown(KeyCode.F))
        {
            UIManager.Instance.ShowPanel(PanelType.Lose);
        }
    }
}
