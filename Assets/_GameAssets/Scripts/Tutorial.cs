using System;
using Lean.Touch;
using UniRx;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
    [SerializeField] private TutorialType type;

    public TutorialType Type => type;

    private void OnEnable()
    {
        if (type != TutorialType.Match && type != TutorialType.Rotate)
        {
            Observable.Timer(TimeSpan.FromSeconds(0.5f)).TakeUntilDisable(this).Subscribe(l => LeanTouch.OnFingerTap += TapScreen);
        }
    }

    private void TapScreen(LeanFinger obj)
    {
        gameObject.SetActive(false);
    }
    private IDisposable rotateTutorialCooldown;

    private void OnDisable()
    {

        LeanTouch.OnFingerTap -= TapScreen;
    }
}