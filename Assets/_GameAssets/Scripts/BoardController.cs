using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Framework.Game.Currency;
using GameAssets.Scripts;
using GameAssets.Scripts.Level;
using UniRx;
using UnityEngine;

public class BoardController : MonoSingleton<BoardController>
{
    public bool CanPlay { get; set; }

    private List<LevelTemplate> loadedLevelTemplates = new List<LevelTemplate>();
    public LevelTemplate CurrentLevelTemplate => loadedLevelTemplates[CurrentSubLevelIndex];
    
    private GameLevel loadedLevel;
    public GameLevel LoadedLevel => loadedLevel;

    private static int currentSubLevelIndex;
    public static int CurrentSubLevelIndex
    {
        get => currentSubLevelIndex;
        set => currentSubLevelIndex = value;
    }
    private void Awake()
    {
        Application.targetFrameRate = 60;
        EventManager.Instance.SubscribeEvent(GameEventType.GameStart, OnGameStart);
    }

    private void OnGameStart()
    {
        if (GameConfig.Instance.cpiMode)
        {
            // LoadCPI();
            return;
        }
        SoundManager.Instance.PlaySound("LevelStart");
        loadedLevel = LevelManager.Instance.GetCurrentLevel();
        for (int i = 0; i < loadedLevel.subLevels.Length; i++)
        {
            var subLevel = Instantiate(loadedLevel.subLevels[i], transform).GetComponent<LevelTemplate>();
            foreach (var editorSlot in subLevel.GetComponentsInChildren<EditorSlot>())
            {
                var slot = SpawnSlot(editorSlot.type);
                // slot.Initialize(editorSlot.type, editorSlot.tileColor);
                Destroy(editorSlot.gameObject);
            }
            loadedLevelTemplates.Add(subLevel);
        }
        StartSubLevel();
    }

    private GameObject SpawnSlot(SlotType t)
    { 
        switch (t)
        {
            // case SlotType.Tile:
                // return Instantiate(References.Instance.tilePrefab, transform).GetComponent<Slot>();
        }

        throw new Exception("Can't find the slot type");
    }

    public void StartSubLevel()
    {
        CanPlay = true;
        // TutorialController.Instance.CheckTutorials();
        TargetController.Instance.SetUpTargets();
    }

    private IDisposable levelCheckDisposable;
    public void CheckLevelStatus()
    {
        levelCheckDisposable?.Dispose();
        levelCheckDisposable = Observable.Timer(TimeSpan.FromSeconds(2f)).TakeUntilDisable(this).Subscribe(delegate(long l)
        {
            if (GameManager.Instance.IsGameEnded)
                return;
            if (TargetController.Instance.IsLevelCompleted())
            {
                CanPlay = false;
                GameManager.Instance.IsGameEnded = true;
                SoundManager.Instance.PlaySound("LevelEnd");
                UIManager.Instance.ShowPanel(PanelType.Win);
                HapticManager.Instance.HapticFeedback(HapticType.Success);
            }
        });
    }
}