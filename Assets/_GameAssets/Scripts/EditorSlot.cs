using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

#if UNITY_EDITOR
using UnityEditor;
#endif


public enum SlotType
{
    Tile
}
[ExecuteInEditMode]
[SelectionBase]
public class EditorSlot : MonoBehaviour
{
    public SlotType type;
    private Vector3 lastPosition;
    void OnValidate()
    {
        UpdateSlots();
    }
    public void UpdateSlots()
    {
        var spriteRenderer = GetComponent<SpriteRenderer>();

        if (type is SlotType.Tile)
        {
            
        }
    }

    void Awake()
    {
        lastPosition = transform.position;
    }
#if UNITY_EDITOR
    void Update()
    {
        var currPos = transform.position;

        if (Selection.gameObjects.Contains(gameObject) == false)
        {
            transform.position = lastPosition;
            return;
        }

        currPos.x = Mathf.Floor(currPos.x);
        currPos.y = Mathf.Floor(currPos.y) + 0.5f;
        currPos.z = 0f;
        transform.position = currPos;

        MoveIfOverlapping();

        lastPosition = transform.position;
    }

    void MoveIfOverlapping()
    {
        var lt = GetComponentInParent<LevelTemplate>();
        if (lt == null)
            return;
        var otherTiles = lt.GetComponentsInChildren<EditorSlot>().Where(t => t != this);
        foreach (var tile in otherTiles)
        {
            var dist = Vector3.Distance(transform.position, tile.transform.position);
            if (dist < 0.99f)
            {
                if (dist < 0.01f)
                {
                    transform.position += Vector3.up;
                }

                var dir = (transform.position - tile.transform.position).normalized;
                transform.position = tile.transform.position + dir * 1;
            }
        }
    }
#endif

}