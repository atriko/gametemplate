using System;
using GameAssets.Scripts;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEngine;

public class RagdollController : MonoBehaviour
{
    private Rigidbody[] rigidbodies;
    private Collider[] colliders;
    private Animator animator;
    
    private void Awake()
    {
        animator = GetComponentInChildren<Animator>();
        rigidbodies = GetComponentsInChildren<Rigidbody>();
        colliders = GetComponentsInChildren<Collider>();
    }

    private void Start()
    {
        DisableRagdoll();
    }

    [Button]
    public void EnableRagdoll()
    {
        animator.enabled = false;
        rigidbodies.ForEach(delegate(Rigidbody r)
        {
            r.isKinematic = false;
        });
        colliders.ForEach(delegate(Collider c)
        {
            c.isTrigger = false;
        });
        Launch(transform.forward);
    }
    
    [Button]
    public void DisableRagdoll()
    {
        animator.enabled = true;
        rigidbodies.ForEach(delegate(Rigidbody r)
        {
            r.isKinematic = true;
        });
        colliders.ForEach(delegate(Collider c)
        {
            c.isTrigger = true;
        });
    }

    public void Launch(Vector3 direction)
    {
        // rigidbodies[0].velocity = direction * GameConfig.Instance.ragdollForwardForce + Vector3.up * GameConfig.Instance.ragdollUpwardForce;
    }
}