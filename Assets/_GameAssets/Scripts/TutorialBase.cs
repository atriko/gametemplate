using System;
using Lean.Touch;
using UniRx;
using UnityEngine;

public class TutorialBase : MonoBehaviour
{
    [SerializeField] protected RectTransform highlight;

    private void OnEnable()
    {
        Observable.Timer(TimeSpan.FromSeconds(0.6f)).TakeUntilDisable(this).Subscribe(delegate(long l)
        {
            LeanTouch.OnFingerDown += TapScreen;
        });
    }

    public virtual void SetHighlightObject(GameObject objectToHighlight)
    {
        highlight.position = UIManager.Instance.WorldToUISpace(objectToHighlight.transform.position);
    }

    private void TapScreen(LeanFinger obj)
    {
        gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        LeanTouch.OnFingerDown -= TapScreen;
    }
}