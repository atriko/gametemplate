using UnityEngine;

public class MuteButton : MonoBehaviour
{
    [SerializeField] private GameObject muted;
    [SerializeField] private GameObject unmuted;
    private void Awake()
    {
        UpdateIcon();
    }
    private void UpdateIcon()
    {
        muted.SetActive(!SoundManager.Instance.SoundEnabled);
        unmuted.SetActive(SoundManager.Instance.SoundEnabled);
    }
    public void OnClick()
    {
        SoundManager.Instance.UpdateSound(!SoundManager.Instance.SoundEnabled);
        UpdateIcon();
    }
}
