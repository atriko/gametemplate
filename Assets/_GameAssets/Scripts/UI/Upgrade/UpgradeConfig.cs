using UnityEngine;

namespace GameAssets.Scripts
{
    [CreateAssetMenu(fileName = "UpgradeConfigs", menuName = "Framework/Game/UpgradeConfigs")]
    public class UpgradeConfig : ScriptableObject
    {
        [SerializeField] private int[] coinRequiredSpeed;
        [SerializeField] private float[] speedUpgradeValues;
        [SerializeField] private int[] coinRequiredPower;
        [SerializeField] private float[] powerUpgradeValues;
        [SerializeField] private int[] coinRequiredIncome;
        [SerializeField] private float[] incomeUpgradeValues;

        public int[] CoinRequiredSpeed => coinRequiredSpeed;
        public int[] CoinRequiredIncome => coinRequiredIncome;
        public int[] CoinRequiredPower => coinRequiredPower;
        public float[] SpeedUpgradeValues => speedUpgradeValues;
        public float[] IncomeUpgradeValues => incomeUpgradeValues;
        public float[] PowerUpgradeValues => powerUpgradeValues;
    }
}