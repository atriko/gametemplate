using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace GameAssets.Scripts
{
    public class UIHudSpawner : MonoBehaviour
    {
        public static UIHudSpawner Instance { get; private set; }
        
        [Title("Properties")]
        [SerializeField] private TextMeshProUGUI textPrefab;
        [SerializeField] private int poolAmount = 100;

        private Camera camera;
        private TextMeshProUGUI[] scoreTexts;

        private void Awake() => Instance = this;
        private void Start()
        {
            camera = Camera.main;
            scoreTexts = new TextMeshProUGUI[poolAmount];
            for (int i = 0; i < poolAmount; i++)
            {
                scoreTexts[i] = Instantiate(textPrefab, transform);
                scoreTexts[i].gameObject.SetActive(false);
            }
        }

        public void SpawnText(Transform target, Vector3 offset, string message, Color color)
        {
            TextMeshProUGUI textMesh = Array.Find(scoreTexts, ugui => !ugui.gameObject.activeSelf);

            if (textMesh != null)
            {
                DOVirtual.Float(0f, 200f, 0.75f, value =>
                {
                    Vector3 position = camera.WorldToScreenPoint(target.position + offset) + Vector3.up * value;
                    textMesh.transform.position = position;
                }).OnStart(() =>
                {
                    textMesh.color = color;
                    textMesh.text = message;

                    textMesh.gameObject.SetActive(true);
                    // textMesh.DOColor(Color.clear, 0.25f).SetDelay(0.75f);
                }).OnComplete(() => textMesh.gameObject.SetActive(false)).SetEase(Ease.Linear);
            }
        }
    }
}