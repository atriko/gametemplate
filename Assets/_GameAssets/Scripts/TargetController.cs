using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class TargetController : MonoSingleton<TargetController>
{
    private List<Target> targets = new List<Target>();

    private HorizontalLayoutGroup layout;
    private bool targetsCompleted;

    private void Awake()
    {
        layout = GetComponent<HorizontalLayoutGroup>();
    }

    public void SetUpTargets()
    {
        
    }

    public void TargetHit(string type)
    {
        if (targetsCompleted)
            return;
        targets.FirstOrDefault(target => target.TargetType == type)?.Hit();
    }

    public bool IsLevelCompleted()
    {
        targetsCompleted = targets.TrueForAll(t => t.IsCompleted);
        return targetsCompleted;
    }
}