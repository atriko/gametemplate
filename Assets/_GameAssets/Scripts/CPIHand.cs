using System;
using DG.Tweening;
using Lean.Touch;
using Sirenix.OdinInspector;
using UniRx;
using UnityEngine;

public class CPIHand : MonoBehaviour
{
    [SerializeField] private Transform hand;
    [SerializeField] private bool rotateOnClick;
    [SerializeField] private bool scaleOnClick;

    [ShowIf("scaleOnClick")] [SerializeField]
    private float scaleTime;

    [ShowIf("scaleOnClick")] [SerializeField]
    private float scaleAmount;

    [ShowIf("rotateOnClick")] [SerializeField]
    private float rotateTime;

    [ShowIf("rotateOnClick")] [SerializeField]
    private float rotateAmount;

    private Tween rotateTween;
    private Tween scaleTween;

    private void Start()
    {
        LeanTouch.OnFingerDown += FingerDown;
        Observable.EveryUpdate().TakeUntilDisable(this).Subscribe(Follow);
    }

    private IDisposable click;

    private void FingerDown(LeanFinger obj)
    {
        if (rotateOnClick)
        {
            rotateTween?.Kill(true);
            rotateTween = hand.transform.DOLocalRotate(new Vector3(0, 0, rotateAmount), rotateTime).SetLoops(2, LoopType.Yoyo);
        }

        if (scaleOnClick)
        {
            scaleTween?.Kill(true);
            scaleTween = hand.transform.DOScale(Vector3.one * scaleAmount, scaleTime).SetLoops(2, LoopType.Yoyo);
        }

        // click?.Dispose();
        // hand.gameObject.SetActive(false);
        // click = Observable.Timer(TimeSpan.FromSeconds(3f)).TakeUntilDisable(this).Subscribe(l => hand.gameObject.SetActive(true));
    }

    private void Follow(long obj)
    {
        var mousePos = Input.mousePosition;
        mousePos.z = 0;
        hand.transform.position = mousePos;
    }

    private void OnDestroy()
    {
        LeanTouch.OnFingerDown -= FingerDown;

    }
}