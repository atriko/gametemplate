using System;
using System.Linq;
using GameAssets.Scripts;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public struct AnimationEventSpawn
{
    public string name;
    public GameObject prefab;
    public Vector3 spawnOffset;
}

public class AnimationEventSpawner : MonoBehaviour
{
    [SerializeField] private UnityEvent customEvent;

    public void SpawnObject(string spawnName)
    {
        var spawn = AnimationSettings.Instance.spawns.FirstOrDefault(spawn => spawn.name == spawnName);
        Instantiate(spawn.prefab, transform.position + spawn.spawnOffset, Quaternion.identity);
        customEvent?.Invoke();
    }
}