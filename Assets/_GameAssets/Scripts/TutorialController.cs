using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;

public enum TutorialType
{
    Info,
    Match,
    Target,
    Moves,
    Obstacle,
    Chain,
    Tower,
    Rotate
}

public class TutorialController : MonoSingleton<TutorialController>
{
    private List<Tutorial> tutorials;

    private void Awake()
    {
        tutorials = GetComponentsInChildren<Tutorial>(true).ToList();

        if (LevelManager.Instance.GetCurrentLevelNumber() == 2 || LevelManager.Instance.GetCurrentLevelNumber() == 3)
        {
            Observable.Timer(TimeSpan.FromSeconds(3f)).TakeUntilDisable(this).Subscribe(l => ShowTutorial(TutorialType.Rotate));
        }
    }

    public void ShowTutorial(TutorialType t)
    {
        if (t == TutorialType.Rotate && PlayerPrefs.GetInt("RotateTutorialShown", 0) == 2)
            return;
        tutorials.FirstOrDefault(tutorial => tutorial.Type == t)?.gameObject.SetActive(true);
    }

    public bool TutorialActive()
    {
        var activeTutorial = tutorials.FirstOrDefault(tutorial => tutorial.gameObject.activeInHierarchy);
        return activeTutorial != null && activeTutorial.Type != TutorialType.Match && activeTutorial.Type != TutorialType.Rotate;
    }


    public void HideTutorial(TutorialType t)
    {
        tutorials.FirstOrDefault(tutorial => tutorial.Type == t)?.gameObject.SetActive(false);
    }

    public bool IsMatchTutorialActive()
    {
        var activeTutorial = tutorials.FirstOrDefault(tutorial => tutorial.gameObject.activeInHierarchy);
        return activeTutorial != null && activeTutorial.Type == TutorialType.Match;
    }

    public bool IsRotateTutorialActive()
    {
        var activeTutorial = tutorials.FirstOrDefault(tutorial => tutorial.gameObject.activeInHierarchy);
        return activeTutorial != null && activeTutorial.Type == TutorialType.Rotate;
    }
}