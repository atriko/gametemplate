using DG.Tweening;
using Sirenix.Utilities;
using UnityEngine;

public class InGameStars : MonoBehaviour
{
    [SerializeField] private GameObject[] stars;

    private Tween scaleTween;
    public void SetStars(int count)
    {
        stars.ForEach(star => star.SetActive(false));
        for (int i = 0; i < count; i++)
        {
            if (!stars[i].activeInHierarchy)
            {
                scaleTween?.Kill(true);
                scaleTween = stars[i].transform.DOPunchScale(Vector3.one * 0.15f, 0.3f);
                stars[i].SetActive(true);
            }
        }
    }
}